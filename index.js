import express from 'express';

import set_place from './set_place.js'

const app=express();

//Ruta, punto de acceso al aplicativo, interfaz de la API

app.get('/greeting', (peticion, respuesta)=>{

    respuesta.send("Hola Backend!!");
});

app.get('/set_place', (peticion, respuesta)=>{

   set_place();
   respuesta.send("Algo paso!!");
  
});

//Inicializar servidor

app.listen(3000,()=>{
    console.log("servidor escuchando en el puerto 3000")

});
