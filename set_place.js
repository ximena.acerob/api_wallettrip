import mysql from 'mysql';

export default function set_place(){

    //1. conectarse a la base de datos

    let parametros={

        host:"localhost",
        user:"user_wallettrip",
        password: "1032",
        database:"db_wallettrip"
    }

    let conexion= mysql.createConnection(parametros);

    conexion.connect((err)=>{
        if(err){
            console.log("Error en la conexion a la base de datos!!");
            console.log(err.message);
            return false;
        }else{
            console.log("Conexión establecida...");
            return true;
        }
    });

    //2. Realizar la inserción de datos

    //let consulta="INSERT INTO place (idplace, name, description) VALUES (2, 'Monserrate','Cerro de Monserrate en los cerros orientales')";

    //conexion.query(consulta);

    //3. Mostrar
    conexion.query('SELECT * FROM place', function (error,results, fields){
        if(error)
           throw error;
        
            results.forEach(result => {
                console.log(result);
            });
    })

    //3. Cerrar la conexión

    conexion.end();

    //return "Insertado!!";
}